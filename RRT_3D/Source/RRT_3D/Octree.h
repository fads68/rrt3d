#pragma once

#include "CoreMinimal.h"
#include <vector>
#include <array>
#include <memory>

/**
 * Octree implementation for RRT 3D
 */

const int MAX_NODES_PER_PARTITION = 10;
const int MAX_DEPTH = 4;

struct Node
{
	FVector _location;
	Node* parent;
	float cost;
};
class Octree;
typedef std::array <Octree*, 8> ChildPtrArray;
typedef std::vector<Node*> NodePtrVector;
typedef std::reference_wrapper<Octree> OctreeRef;

class RRT_3D_API Octree
{
public:
	Octree();
	~Octree();

	Octree(FVector center, float extent, Octree* parent = nullptr);
	void AddNode(Node* toAdd);
	OctreeRef GetPartitionByIndex(int index) { return *_partitions[index]; }
	OctreeRef GetPartition(FVector location);
	Node* GetNearestNode(FVector location);
	Node* GetLowestCostNode(FVector location);
	NodePtrVector GetObjects() { return _objects; }
	NodePtrVector GetObjectsRecursive();
	void Clear();
private:
	/*
	* mapped as follows:
	* 0 = +x +y +z
	* 1 = -x +y +z
	* 2 = +x -y +z
	* 3 = -x -y +z
	* 4 = +x +y -z
	* 5 = -x +y -z
	* 6 = +x -y -z
	* 7 = -x -y -z
	*/
	int GetPartitionByPos(FVector location);
	ChildPtrArray _partitions;
	NodePtrVector _objects;
	Octree* _parent;
	FVector _center;
	float _extent;
	int _depth;
};
