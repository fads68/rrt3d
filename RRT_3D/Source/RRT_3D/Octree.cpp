#include "Octree.h"

Octree::Octree()
{
	std::fill(_partitions.begin(), _partitions.end(), nullptr);
}

Octree::~Octree()
{
}

Octree::Octree(FVector center, float extent, Octree* parent)
{
	_center = center;
	_extent = extent;
	_parent = parent;
	std::fill(_partitions.begin(), _partitions.end(), nullptr);
	if (parent)
	{
		_depth = parent->_depth + 1;
	}
	else
	{
		_depth = 0;
	}
}

void Octree::AddNode(Node* toAdd)
{
	int index = GetPartitionByPos(toAdd->_location);
	if (_partitions[index] != nullptr)
	{
		_partitions[index]->AddNode(toAdd);
	}
	else
	{
		_objects.push_back(toAdd);
		if (_objects.size() >= MAX_NODES_PER_PARTITION && _depth != MAX_DEPTH)
		{
			float newExtent = _extent / 2;
			float qExtent = newExtent / 2;
			_partitions[0] = new Octree(FVector(_center.X + qExtent, _center.Y + qExtent, _center.Z + qExtent), newExtent, this);
			_partitions[1] = new Octree(FVector(_center.X - qExtent, _center.Y + qExtent, _center.Z + qExtent), newExtent, this);
			_partitions[2] = new Octree(FVector(_center.X + qExtent, _center.Y - qExtent, _center.Z + qExtent), newExtent, this);
			_partitions[3] = new Octree(FVector(_center.X - qExtent, _center.Y - qExtent, _center.Z + qExtent), newExtent, this);
			_partitions[4] = new Octree(FVector(_center.X + qExtent, _center.Y + qExtent, _center.Z - qExtent), newExtent, this);
			_partitions[5] = new Octree(FVector(_center.X - qExtent, _center.Y + qExtent, _center.Z - qExtent), newExtent, this);
			_partitions[6] = new Octree(FVector(_center.X + qExtent, _center.Y - qExtent, _center.Z - qExtent), newExtent, this);
			_partitions[7] = new Octree(FVector(_center.X - qExtent, _center.Y - qExtent, _center.Z - qExtent), newExtent, this);

			for(Node* node : _objects)
			{
				AddNode(node);
			}

			_objects.clear();
		}
	}
}

OctreeRef Octree::GetPartition(FVector location)
{
	int index = GetPartitionByPos(location);
	if (_partitions[index] != nullptr)
	{
		return _partitions[index]->GetPartition(location);
	}
	return *this;
}

Node* Octree::GetNearestNode(FVector location)
{
	int index = GetPartitionByPos(location);
	if (_partitions[index] != nullptr)
	{
		return _partitions[index]->GetNearestNode(location);
	}
	else if (_parent != nullptr)
	{
		float minDist = INT_MAX;
		Node* minNode = nullptr;
		
		for (int i = 0; i < 8; ++i)
		{
			Octree child = _parent->GetPartitionByIndex(i);
			NodePtrVector childObjs = child.GetObjects();

			for (Node* n : childObjs)
			{
				float dist = FVector::DistSquared(location, n->_location);
				if (dist < minDist)
				{
					minDist = dist;
					minNode = n;
				}
			}
		}
		return minNode;
	}
	else
	{
		float minDist = INT_MAX;
		Node* minNode = nullptr;
		for (Node* n : _objects)
		{
			float dist = FVector::DistSquared(location, n->_location);
			if (dist < minDist)
			{
				minDist = dist;
				minNode = n;
			}
		}

		return minNode;
	}

}

Node* Octree::GetLowestCostNode(FVector location)
{
	int index = GetPartitionByPos(location);
	if (_partitions[index] != nullptr)
	{
		return _partitions[index]->GetNearestNode(location);
	}
	else if (_parent != nullptr)
	{
		float minCost = INT_MAX;
		Node* minNode = nullptr;

		for (int i = 0; i < 8; ++i)
		{
			Octree child = _parent->GetPartitionByIndex(i);
			NodePtrVector childObjs = child.GetObjects();

			for (Node* n : childObjs)
			{
				float dist = FVector::DistSquared(location, n->_location);
				if (n->cost + dist < minCost && dist > 0)
				{
					minCost = n->cost + dist;
					minNode = n;
				}
			}
		}
		return minNode;
	}
	else
	{
		float minDist = INT_MAX;
		Node* minNode = nullptr;
		for (Node* n : _objects)
		{
			float dist = FVector::DistSquared(location, n->_location);
			if (dist < minDist && dist > 0)
			{
				minDist = dist;
				minNode = n;
			}
		}

		return minNode;
	}

}

NodePtrVector Octree::GetObjectsRecursive()
{
	NodePtrVector vec;
	for (Node* node : _objects)
	{
		vec.push_back(node);
	}

	if (_partitions[0] != nullptr)
	{
		for (auto oct : _partitions)
		{
			for (Node* node : oct->GetObjectsRecursive())
			{
				vec.push_back(node);
			}
		}
	}
	return vec;

}

void Octree::Clear()
{
	for (Node* node : _objects)
	{
		delete node;
	}
	_objects.clear();
	for (int i = 0; i < 8; ++i)
	{
		if (_partitions[i] != nullptr)
		{
			delete _partitions[i];
			_partitions[i] = nullptr;
		}
	}
}


int Octree::GetPartitionByPos(FVector location)
{
	// +z
	if (location.Z >= _center.Z)
	{
		//+Y
		if (location.Y >= _center.Y)
		{
			//+X
			if (location.X >= _center.X)
			{
				return 0;
			}
			//-X
			else
			{
				return 1;
			}
		}
		//-Y
		else
		{
			//+X
			if (location.X >= _center.X)
			{
				return 2;
			}
			//-X
			else
			{
				return 3;
			}
		}
	}
	//-z
	else
	{
		//+Y
		if (location.Y >= _center.Y)
		{
			//+X
			if (location.X >= _center.X)
			{
				return 4;
			}
			//-X
			else
			{
				return 5;
			}
		}
		//-Y
		else
		{
			//+X
			if (location.X >= _center.X)
			{
				return 6;
			}
			//-X
			else
			{
				return 7;
			}
		}
	}
}


