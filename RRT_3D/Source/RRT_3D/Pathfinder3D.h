
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Octree.h"
#include "Engine/World.h"
#include <vector>
#include "Pathfinder3D.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RRT_3D_API UPathfinder3D : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPathfinder3D();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector PathableSpaceCenter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector PathableSpaceExtents;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PawnRadius;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int PathOptimizationLoops;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool VisualizePath;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool SweepAgentRadius;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool InformOptimizationLoops;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool CallOnTick;

	bool FindPath(FVector destination);
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	Octree* _tree;
	Node* Explore();
	bool TestForObstruction(FVector begin, FVector end);
	void FollowPathRecursive(Node* end);
	void DrawTree();
	std::vector<Node*> _path;
	void CalculatePathBounds();
	float CalcHeuristic(FVector to, FVector from);
	Node* start;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
		
};
