// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RRT_3DGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class RRT_3D_API ARRT_3DGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
