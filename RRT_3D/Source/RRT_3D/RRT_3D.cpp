// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "RRT_3D.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, RRT_3D, "RRT_3D" );
