#include "Pathfinder3D.h"
#include "DrawDebugHelpers.h"
#include <vector>
#include "Kismet/KismetSystemLibrary.h"

// Sets default values for this component's properties
UPathfinder3D::UPathfinder3D()
{
	PrimaryComponentTick.bCanEverTick = true;	
}




// Called when the game starts
void UPathfinder3D::BeginPlay()
{
	//initializations
	Super::BeginPlay();
	float max = FMath::Max3(PathableSpaceExtents.X, PathableSpaceExtents.Y, PathableSpaceExtents.Z);
	_tree = new Octree(PathableSpaceCenter, max);
	
	//grab a point at random
	FVector randVec;
	randVec.X = FMath::FRandRange(-1000, 1000);
	randVec.Y = FMath::FRandRange(-1000, 1000);
	randVec.Z = FMath::FRandRange(-1000, 1000);

	//pathfind
	while (!FindPath(randVec))
	{
		//if that point didn't have a valid path, try again
		randVec.X = FMath::FRandRange(-1000, 1000);
		randVec.Y = FMath::FRandRange(-1000, 1000);
		randVec.Z = FMath::FRandRange(-1000, 1000);

	}
	
}



// Called every frame
void UPathfinder3D::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (CallOnTick)
	{
		for (int i = 0; i < 4; ++i)
		{
			FVector randVec;
			randVec.X = FMath::FRandRange(-1000, 1000);
			randVec.Y = FMath::FRandRange(-1000, 1000);
			randVec.Y = FMath::FRandRange(-1000, 1000);
			randVec.Z = FMath::FRandRange(-1000, 1000);
			while (!FindPath(randVec))
			{
				randVec.X = FMath::FRandRange(-1000, 1000);
				randVec.Y = FMath::FRandRange(-1000, 1000);
				randVec.Z = FMath::FRandRange(-1000, 1000);
			}
		}
		UE_LOG(LogTemp, Warning, TEXT("frame time was %f"), DeltaTime);
		FlushPersistentDebugLines(GetWorld());
	}
	
	
	
}

bool UPathfinder3D::FindPath(FVector destination)
{
	//clear tree and create a new one
	_tree->Clear();

	// do a check for invalid end position
	TArray<TEnumAsByte<EObjectTypeQuery> > ObjectsToTraceAsByte;
	ObjectsToTraceAsByte.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldStatic));
	ObjectsToTraceAsByte.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldDynamic));
	TArray<AActor*> toIgnore;
	toIgnore.Add(GetOwner());
	TArray<AActor*> outActors;

	if (UKismetSystemLibrary::SphereOverlapActors(GetWorld(), destination, PawnRadius, ObjectsToTraceAsByte, NULL, toIgnore, outActors))
	{
		return false;
	}

	start = new Node();
	start->parent = nullptr;
	start->_location = GetOwner()->GetActorLocation();
	start->cost = 0;
	_tree->AddNode(start);


	int tempOptimizationNum = PathOptimizationLoops;

	//do preliminary check for end
	bool endFound = !TestForObstruction(start->_location, destination);
	if (endFound)
	{
		tempOptimizationNum = 0;
	}

	Node* end = new Node();
	end->_location = destination;

	Node* current = start;
	while (!endFound)
	{
		current = nullptr;
		//loop until we've made a new valid node that connects to our tree
		while (current == nullptr)
		{
			current = Explore();
		}

		endFound = !TestForObstruction(current->_location, destination);
	
	}

	end->parent = current;
	end->cost = end->parent->cost + CalcHeuristic(end->_location, end->parent->_location);
	_tree->AddNode(end);

	_path.clear();
	FollowPathRecursive(end);

	FVector storeExtents = PathableSpaceExtents;
	FVector storeCenter = PathableSpaceCenter;
	CalculatePathBounds();
	//optomization loops
	for (int i = 0; i < tempOptimizationNum; ++i)
	{
		
		current = nullptr;
		//loop until we've made a new valid node that connects to our tree
		while (current == nullptr)
		{
			current = Explore();
		}
		if (!TestForObstruction(current->_location, destination) && current->cost < end->parent->cost)
		{
			end->parent = current;
			end->cost = end->parent->cost + CalcHeuristic(end->_location, end->parent->_location);
		}

		/*
		* Informs optomization loops
		*/
		if (InformOptimizationLoops)
		{
			
			//clear our path then add our new one
			_path.clear();
			FollowPathRecursive(end);
			//set our pathable space to a bounding box around the path
			CalculatePathBounds();
		}
	}

	//clear our path then add our new one
	_path.clear();
	FollowPathRecursive(end);

	if (VisualizePath)
	{
		DrawTree();
	}

	return true;
}

Node* UPathfinder3D::Explore()
{
	FVector randPos;
	bool hit = true;
	
	//loop until we find a valid spot
	while (hit)
	{
		//generate a random position
		randPos.X = FMath::RandRange(PathableSpaceCenter.X - PathableSpaceExtents.X, PathableSpaceCenter.X + PathableSpaceExtents.X);
		randPos.Y = FMath::RandRange(PathableSpaceCenter.Y - PathableSpaceExtents.Y, PathableSpaceCenter.Y + PathableSpaceExtents.Y);
		randPos.Z = FMath::RandRange(PathableSpaceCenter.Z - PathableSpaceExtents.Z, PathableSpaceCenter.Z + PathableSpaceExtents.Z);

		
		//check sphere overlap
		TArray<TEnumAsByte<EObjectTypeQuery> > ObjectsToTraceAsByte;
		ObjectsToTraceAsByte.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldStatic));
		ObjectsToTraceAsByte.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldDynamic));
		TArray<AActor*> toIgnore;
		toIgnore.Add(GetOwner());
		TArray<AActor*> outActors;

		hit = UKismetSystemLibrary::SphereOverlapActors(GetWorld(), randPos, PawnRadius, ObjectsToTraceAsByte, NULL, toIgnore, outActors);
	}
	//once we have a valid position, check the start
	if (!TestForObstruction(randPos, start->_location))
	{
		float tempCost = CalcHeuristic(randPos, start->_location);
		Node* created = new Node();
		created->parent = start;
		created->_location = randPos;
		created->cost = tempCost;
		_tree->AddNode(created);
		return created;
	}
	//if start doesn't work, check nearest
	Node* nearest = _tree->GetLowestCostNode(randPos);
	if (nearest != nullptr)
	{
		//then see if we have a clear path to that node
		hit = TestForObstruction(randPos, nearest->_location);
		//if we didn't hit anything then we have a clear path to it
		if (!hit)
		{
			Node* created = new Node();
			created->parent = nearest;
			created->_location = randPos;
			created->cost = created->parent->cost + CalcHeuristic(created->_location, created->parent->_location);
			_tree->AddNode(created);
			return created;
		}
	}
	
	return nullptr;
}

bool UPathfinder3D::TestForObstruction(FVector begin, FVector end)
{
	//trace boilerplate
	TArray<TEnumAsByte<EObjectTypeQuery> > ObjectsToTraceAsByte;
	ObjectsToTraceAsByte.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldStatic));
	ObjectsToTraceAsByte.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldDynamic));
	FCollisionQueryParams params(FName("CustomTrace"), false, GetOwner());
	FHitResult castHit;

	if (SweepAgentRadius)
	{
		if (GetWorld()->SweepSingleByObjectType(castHit, begin, end, FQuat::Identity, ObjectsToTraceAsByte, FCollisionShape::MakeSphere(PawnRadius), params))
		{
			if (castHit.GetActor() == GetOwner())
			{
				return false;
			}
			return true;
		}
		return false;
	}
	else
	{
		if (GetWorld()->LineTraceSingleByObjectType(castHit, begin, end, ObjectsToTraceAsByte, params))
		{
			if (castHit.GetActor() == GetOwner())
			{
				return false;
			}
			return true;
		}
		return false;
	}
}

void UPathfinder3D::FollowPathRecursive(Node* end)
{
	if (end->parent != nullptr)
	{
		FollowPathRecursive(end->parent);
	}
	_path.push_back(end);
}

void UPathfinder3D::DrawTree()
{
	NodePtrVector vector = _tree->GetObjectsRecursive(); 
	for (Node* node : vector)
	{
		if (node->parent != nullptr)
		{
			DrawDebugLine(GetWorld(), node->parent->_location, node->_location, FColor::Red, true, 0.05f, (uint8)'\000', 10.0f);
		}
	}
	for (Node* node : _path)
	{
		if (node->parent != nullptr)
		{
			DrawDebugLine(GetWorld(), node->parent->_location, node->_location, FColor::Blue, true, 0.05f, (uint8)'\000', 15.0f);
		}
	}
}

void UPathfinder3D::CalculatePathBounds()
{
	FVector avg = FVector::ZeroVector;
	FVector max = FVector::ZeroVector;

	for (Node* node : _path)
	{
		avg += node->_location;

		if (abs(node->_location.X) > max.X)
		{
			max.X = abs(node->_location.X);
		}	
		if (abs(node->_location.Y) > max.Y)
		{
			max.Y = abs(node->_location.Y);
		}
		if (abs(node->_location.Z) > max.Z)
		{
			max.Z = abs(node->_location.Z);
		}
	}
	
	avg /= _path.size();
	PathableSpaceCenter = avg;
	PathableSpaceExtents = avg - max;
}

float UPathfinder3D::CalcHeuristic(FVector to, FVector from)
{
	float dx = abs(from.X - to.X );
	float dy = abs(from.Y - to.Y);
	float dz = abs(from.Z - to.Z);
	return (dx + dy + dz);
}

